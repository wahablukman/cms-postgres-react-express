const db = require('../db/connection');

module.exports = {
  findCompanyProfile() {
    return db('companyprofile').first();
  },
  create(companyProfile) {
    return db('companyprofile').insert(companyProfile, '*');
  },
  update(companyProfile) {
    return db('companyprofile')
      .first()
      .update(companyProfile, '*');
  }
};
