const db = require('../db/connection');
const setupPaginator = require('knex-paginator');
setupPaginator(db);

// query functions
module.exports = {
  findAll(queryPage, querySort) {
    return db
      .select(
        'post.id',
        'post.image',
        'post.title',
        'post.slug',
        'post.content',
        'post.view_count',
        'post.featured',
        'post.created_at',
        'post.updated_at',
        'post.user_id',
        'users.first_name as user_first_name',
        'users.last_name as user_last_name',
        'users.avatar as user_avatar'
      )
      .from('post')
      .innerJoin('users', 'post.user_id', 'users.id')
      .orderBy('post.created_at', querySort)
      .paginate(5, queryPage, true);
  },
  findByID2(id) {
    return db
      .select('*')
      .from('post')
      .where('post.id', id)
      .first();
  },
  findByID(id) {
    return db
      .select(
        'post.id',
        'post.image',
        'post.title',
        'post.slug',
        'post.content',
        'post.view_count',
        'post.featured',
        'post.created_at',
        'post.updated_at',
        'post.user_id',
        'users.first_name as user_first_name',
        'users.last_name as user_last_name',
        'users.avatar as user_avatar'
      )
      .from('post')
      .innerJoin('users', 'post.user_id', 'users.id')
      .where('post.id', id)
      .first();
  },
  findBySlug(slug) {
    return db
      .select(
        'post.id',
        'post.image',
        'post.title',
        'post.slug',
        'post.content',
        'post.view_count',
        'post.featured',
        'post.created_at',
        'post.updated_at',
        'post.user_id',
        'users.first_name as user_first_name',
        'users.last_name as user_last_name',
        'users.avatar as user_avatar'
      )
      .from('post')
      .innerJoin('users', 'post.user_id', 'users.id')
      .where('post.slug', slug)
      .first();
  },
  create(post) {
    return db('post').insert(post, '*');
  },
  update(id, post) {
    return db('post')
      .where('id', id)
      .update(post, '*');
  },
  delete(id) {
    return db('post')
      .where('id', id)
      .del();
  }
};
