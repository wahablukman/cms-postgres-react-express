const db = require('../db/connection');
const setupPaginator = require('knex-paginator');
const moment = require('moment');
setupPaginator(db);

// query functions
module.exports = {
  findAll(queryPage, querySort) {
    return db('event')
      .orderBy('created_at', querySort)
      .paginate(5, queryPage, true);
  },
  findAllActive() {
    return db('event').where('start_at', '>=', moment());
  },
  findAllAcceptVolunteer() {
    return db('event').where('accept_volunteer', true);
  },
  findByID(id) {
    return db('event')
      .where('id', id)
      .first();
  },
  findBySlug(slug) {
    return db('event')
      .where('slug', slug)
      .first();
  },
  create(event) {
    return db('event').insert(event, '*');
  },
  update(id, event) {
    return db('event')
      .where('id', id)
      .update(event, '*');
  },
  delete(id) {
    return db('event')
      .where('id', id)
      .del();
  }
};
