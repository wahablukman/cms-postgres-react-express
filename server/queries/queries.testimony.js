const db = require('../db/connection');

// query functions
module.exports = {
  findAll() {
    return db('testimony');
  },
  findByID(id) {
    return db('testimony')
      .where('id', id)
      .first();
  },
  create(testimony) {
    return db('testimony').insert(testimony, '*');
  },
  update(id, testimony) {
    return db('testimony')
      .where('id', id)
      .update(testimony, '*');
  },
  delete(id) {
    return db('testimony')
      .where('id', id)
      .del();
  }
};
