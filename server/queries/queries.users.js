const db = require('../db/connection');

// query functions
module.exports = {
  findAll() {
    return db.select('*').from('users');
  },
  findByEmail(email) {
    return db
      .select(
        'users.id',
        'users.username',
        'users.first_name',
        'users.last_name',
        'users.email',
        'users.password',
        'users.avatar',
        'users.created_at',
        'users.updated_at',
        'users.role_id',
        'role.id as role_role_id',
        'role.name as role_name',
        'role.created_at as role_created_at',
        'role.updated_at as role_updated_at'
      )
      .from('users')
      .innerJoin('role', 'users.role_id', 'role.id')
      .where('users.email', email)
      .first();
  },
  findByID(id) {
    return db
      .select(
        'users.id',
        'users.username',
        'users.first_name',
        'users.last_name',
        'users.email',
        'users.password',
        'users.avatar',
        'users.created_at',
        'users.updated_at',
        'users.role_id',
        'role.id as role_role_id',
        'role.name as role_name',
        'role.created_at as role_created_at',
        'role.updated_at as role_updated_at'
      )
      .from('users')
      .innerJoin('role', 'users.role_id', 'role.id')
      .where('users.id', id)
      .first();
  },
  create(user) {
    return db('users').insert(user, '*');
  },
  update(id, user) {
    return db('users')
      .where('id', id)
      .update(user, '*');
  }
};
