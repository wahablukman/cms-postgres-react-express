exports.seed = async (knex, Promise) => {
  // Deletes ALL existing entries
  await knex('companyprofile').del();
  return await knex('companyprofile').insert([
    {
      id: 1,
      name: 'Example Org.',
      logo: 'https://cdn.svgporn.com/logos/poeditor.svg',
      email: 'org@example.com',
      facebook: 'facebook link',
      instagram: 'instagram link',
      youtube: 'youtube link'
    }
  ]);
};
