exports.seed = async (knex, Promise) => {
  // Deletes ALL existing entries
  await knex('role').del();
  await knex.raw('ALTER SEQUENCE role_id_seq restart with 2;');
  return knex('role').insert([
    { id: 1, name: 'Administrator' },
    { id: 2, name: 'Staff' }
  ]);
};
