exports.seed = async (knex, Promise) => {
  const bcrypt = require('bcryptjs');
  // Deletes ALL existing entries
  const password = bcrypt.hashSync('famous', 10);

  await knex('users').del();
  return knex('users').insert([
    {
      id: 1,
      username: 'wahablukman',
      first_name: 'Wahab',
      last_name: 'Saad',
      email: 'wahab22393@gmail.com',
      avatar: 'https://custom-cms.s3.amazonaws.com/images/1562222917820.jpg',
      password,
      role_id: 1
    }
  ]);
};
