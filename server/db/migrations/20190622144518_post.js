exports.up = (knex, Promise) => {
  return knex.schema.createTable('post', table => {
    table.increments('id').primary();
    table.text('image');
    table.text('title').notNullable();
    table.text('slug').notNullable();
    table.string('content', [10000000]).notNullable();
    table.integer('view_count').defaultTo(0);
    table.boolean('featured').defaultTo(false);
    // table
    //   .integer('user_id')
    //   .references('id')
    //   .inTable('users');
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('post');
};
