exports.up = (knex, Promise) => {
  return knex.schema.createTable('role', table => {
    table.increments('id').primary();
    table.text('name');
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('role');
};
