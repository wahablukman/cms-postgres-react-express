exports.up = (knex, Promise) => {
  return knex.schema.table('post_category', table => {
    table
      .integer('post_id')
      .references('id')
      .inTable('post');
    table
      .integer('category_id')
      .references('id')
      .inTable('category');
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.table('post_category', table => {
    table.dropColumn('post_id');
    table.dropColumn('category_id');
  });
};
