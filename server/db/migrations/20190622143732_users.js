exports.up = (knex, Promise) => {
  return knex.schema.createTable('users', table => {
    table.increments('id').primary();
    table.text('username').notNullable();
    table.text('first_name');
    table.text('last_name');
    table.text('email').notNullable();
    table.text('password').notNullable();
    table.string('avatar');
    // table
    //   .integer('role_id')
    //   .references('id')
    //   .inTable('role');
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('users');
};
