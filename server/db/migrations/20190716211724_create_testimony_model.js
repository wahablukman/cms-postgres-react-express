exports.up = (knex, Promise) => {
  return knex.schema.createTable('testimony', table => {
    table.increments('id').primary();
    table.text('name');
    table.text('avatar');
    table.string('testimony', [10000000]);
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('testimony');
};
