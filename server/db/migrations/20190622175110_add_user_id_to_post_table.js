exports.up = (knex, Promise) => {
  return knex.schema.table('post', table => {
    table
      .integer('user_id')
      .references('id')
      .inTable('users');
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.table('post', table => {
    table.dropColumn('user_id');
  });
};
