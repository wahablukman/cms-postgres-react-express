exports.up = (knex, Promise) => {
  return knex.schema.createTable('event', table => {
    table.increments('id').primary();
    table.text('image');
    table.text('title').notNullable();
    table.text('slug').notNullable();
    table.text('place').notNullable();
    table.string('description', [10000000]);
    table.datetime('start_at').notNullable();
    table.datetime('finish_at').notNullable();
    table.boolean('accept_volunteer').defaultTo(false);
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('event');
};
