exports.up = (knex, Promise) => {
  return knex.schema.createTable('companyprofile', table => {
    table.increments('id').primary();
    table.text('name');
    table.text('logo');
    table.text('email');
    table.text('facebook');
    table.text('instagram');
    table.text('youtube');
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('companyprofile');
};
