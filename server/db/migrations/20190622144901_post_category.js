exports.up = (knex, Promise) => {
  return knex.schema.createTable('post_category', table => {
    table.increments('id').primary();
    // table
    //   .integer('post_id')
    //   .references('id')
    //   .inTable('post');
    // table
    //   .integer('category_id')
    //   .references('id')
    //   .inTable('category');
    table.timestamps(true, true);
  });
};

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('post_category');
};
