const express = require('express');
const router = express.Router();
const users = require('../../queries/queries.users');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const upload = require('../../config/imageUpload');
let yup = require('yup');

// GET method
// Test
// Public
router.get('/', async (req, res) => {
  try {
    const allUser = await users.findAll();
    return res.json(allUser);
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// GET method
// Test
// Public
router.get(
  '/user',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const user = await users.findByID(req.user.id);
      if (user) {
        return res.json(user);
      } else {
        return res.status(404).json({
          message: 'Cannot found any user'
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

// POST method
// Login User
// Public
router.post('/login', async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;

  //find user by email
  try {
    const findUser = await users.findByEmail(email);
    if (!findUser) {
      return res.status(404).json({ message: 'user not found' });
    } else {
      // check password
      const matchPassword = await bcrypt.compare(password, findUser.password);
      if (matchPassword) {
        // create jwt payload
        const payload = {
          id: findUser.id,
          username: findUser.username,
          firstName: findUser.first_name,
          lastName: findUser.last_name,
          avatar: findUser.avatar,
          role_name: findUser.role_name
        };

        // Sign token - 604800 = a week
        jwt.sign(
          payload,
          process.env.SECRET_OR_KEY,
          { expiresIn: 604800 },
          (err, token) => {
            res.json({
              success: true,
              token: 'Bearer ' + token
            });
          }
        );
      }
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// POST method
// Register user
// Public
// So far is DONE
router.post('/register', async (req, res) => {
  try {
    // find if user exists
    const userExists = await users.findByEmail(req.body.email);
    const userData = req.body;

    // schema validation
    const schema = yup.object().shape({
      username: yup.string().required(),
      first_name: yup.string(),
      last_name: yup.string(),
      email: yup.string().email({ minDomainSegments: 2 }),
      password: yup.string().required(),
      role_id: yup.number().integer()
    });
    try {
      await schema.validate(userData);
    } catch (validationErrors) {
      return res.json(validationErrors);
    }

    if (userExists) {
      // error
      return res.status(400).json({
        message: 'email already exists'
      });
    } else {
      // create user
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(userData.password, salt, async (err, hash) => {
          if (err) throw err;
          userData.password = hash;
          const createUser = await users.create(userData);
          console.log('user created');
          return res.json(createUser[0]);
        });
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// PUT method
// edit user details
// private
//
router.put(
  '/user',
  upload.single('avatar'),
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    // return console.log(req.user.id);
    try {
      const user = await users.findByID(req.user.id);

      if (user) {
        const userData = {
          username: req.body.username || user.username,
          first_name: req.body.first_name || user.first_name,
          last_name: req.body.last_name || user.last_name,
          email: user.email,
          password: user.password,
          role_id: user.role_id
        };
        if (req.file) {
          userData.avatar = req.file.location;
        } else {
          userData.avatar = user.avatar;
        }

        // validation
        const schema = yup.object().shape({
          username: yup.string().required(),
          first_name: yup.string(),
          last_name: yup.string(),
          email: yup.string().email({ minDomainSegments: 2 }),
          password: yup.string().required(),
          role_id: yup.number().integer()
        });
        try {
          await schema.validate(userData);
        } catch (validationError) {
          return res.status(400).json(validationError);
        }

        console.log(userData);

        const updateUser = await users.update(req.user.id, userData);
        const findUpdatedUser = await users.findByID(updateUser[0].id);
        return res.json(findUpdatedUser);
      } else {
        return res.status(404).json({
          message: 'No user found to be edited'
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

// GET Method
// retrieve current user jwt payload
// private
//
router.get(
  '/current-user',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    let user = await req.user;
    if (user) {
      user = await users.findByID(user.id);
      return res.json(user);
    } else {
      return res.status(500).json({
        message: 'bad error'
      });
    }
  }
);

module.exports = router;
