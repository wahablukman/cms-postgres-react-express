const express = require('express');
const router = express.Router();
const passport = require('passport');
const testimony = require('../../queries/queries.testimony');
const verification = require('../../config/verification');
const imageUpload = require('../../config/imageUpload');
let yup = require('yup');

// GET Method
// Public
// Get all testimonies
//
router.get('/', async (req, res) => {
  try {
    const testimonies = await testimony.findAll();
    return res.json(testimonies);
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// GET Method
// Public
// Get testimony by ID
//
router.get('/:id', async (req, res) => {
  try {
    const findTestimony = testimony.findByID(req.params.id);
    if (findTestimony) {
      return res.json(findTestimony);
    } else {
      return res.status(404).json({
        message: 'Cannot find testimony by that ID'
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// POST Method
// Private
// Create a testimony
//
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  imageUpload.single('avatar'),
  async (req, res) => {
    let testimonyData = {
      name: req.body.name,
      testimony: req.body.testimony
    };

    // if user uploads avatar
    if (req.file) {
      testimonyData.avatar = req.file.location;
    } else {
      testimonyData.avatar = 'https://img.icons8.com/bubbles/2x/user.png';
    }

    // schema validation
    const schema = yup.object().shape({
      name: yup.string().required(),
      testimony: yup.string().required(),
      avatar: yup.string().nullable()
    });
    try {
      await schema.validate(testimonyData);
    } catch (validationError) {
      return res.status(400).json(validationError);
    }

    // Create testimony
    try {
      const createTestimony = await testimony.create(testimonyData);
      return res.json(createTestimony);
    } catch (error) {
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

// PUT Method
// Private
// Edit a testimony by ID
//
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  imageUpload.single('avatar'),
  async (req, res) => {
    try {
      const findTestimony = await testimony.findByID(req.params.id);
      if (!findTestimony) {
        return res
          .status(404)
          .json({ message: 'Cannot find a testimony to be edited' });
      }

      let testimonyData = {
        id: findTestimony.id,
        name: req.body.name || findTestimony.name,
        testimony: req.body.testimony || findTestimony.testimony
      };

      // if user uploads avatar
      if (req.file) {
        testimonyData.avatar = req.file.location;
      } else {
        testimonyData.avatar = testimonyData.avatar;
      }

      // Schema validation
      const schema = yup.object().shape({
        id: yup.required(),
        name: yup.string().required(),
        testimony: yup.string().required(),
        avatar: yup.string().nullable()
      });
      try {
        await schema.validate(testimonyData);
      } catch (validationError) {
        return res.status(400).json(validationError);
      }

      // update testimony
      try {
        const updateTestimony = await testimony.update(
          findTestimony.id,
          testimonyData
        );
        const findUpdatedTestimony = await testimony.findByID(
          updateTestimony[0].id
        );
        return res.json(findUpdatedTestimony);
      } catch (error) {
        return res
          .status(500)
          .json({ message: 'Cannot update Testimony, server error' });
      }
    } catch (error) {
      return res.status(500).json({ message: 'Server Error' });
    }
  }
);

// DELETE Method
// Private
// Delete a testimony by ID
//
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  async (req, res) => {
    try {
      const findTestimony = await testimony.findByID(req.params.id);
      if (!findTestimony) {
        return res
          .status(404)
          .json({ message: 'Cannot find testimony to be deleted' });
      }

      try {
        await testimony.delete(req.params.id);
        return res.json({ message: 'Testimony succesfully deleted' });
      } catch (error) {
        return res
          .status(500)
          .json({ message: 'Cannot delete testimony, server error' });
      }
    } catch (error) {
      return res.status(500).json({ message: 'Server Error' });
    }
  }
);

module.exports = router;
