const express = require('express');
const router = express.Router();
const passport = require('passport');
const post = require('../../queries/queries.post');
let yup = require('yup');
const uuid = require('uuid/v4');
const upload = require('../../config/imageUpload');

// multer file upload test
router.post('/image-upload', upload.single('image'), async (req, res) => {
  let testData = {
    coba: 'hm'
  };
  if (req.file) {
    testData.image = req.file;
  }

  testData.id = 1;

  return res.json(testData);
});

// GET Method
// get all posts
// Public
// make the query
router.get('/', async (req, res) => {
  let queryPage = req.query.pg;
  let querySort = req.query.srt;
  queryPage ? (queryPage = queryPage) : (queryPage = 1);
  querySort ? (querySort = querySort) : (querySort = 'desc');

  try {
    const posts = await post.findAll(queryPage, querySort);
    return res.json(posts);
  } catch (error) {
    return res.status(500).json({
      message: 'Server error'
    });
  }
});

// GET Method
// get a post by ID
// Public
// make the query
router.get('/:id', async (req, res) => {
  try {
    const findPost = await post.findByID(req.params.id);
    if (findPost) {
      return res.json(findPost);
    } else {
      return res.status(404).json({
        message: 'there is no post found with that ID'
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server error'
    });
  }
});

// GET Method
// get a post by Slug
// Public
// make the query
router.get('/slug/:slug', async (req, res) => {
  try {
    const findPost = await post.findBySlug(req.params.slug);
    if (findPost) {
      return res.json(findPost);
    } else {
      return res.status(404).json({
        message: 'there is no post found with that slug'
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server error'
    });
  }
});

// POST Method
// create a post
// Private
// better double slug loop
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  upload.single('image'),
  async (req, res) => {
    try {
      const { title, content } = req.body;

      const postData = {
        title,
        content,
        view_count: 0,
        featured: false,
        user_id: req.user.id
      };

      // if image is uploaded
      if (req.file) {
        postData.image = req.file.location;
      } else {
        postData.image = null;
      }

      // check if slug exists
      let slug = title.replace(/[\W_]+/g, '-').toLowerCase();
      const postExists = await post.findBySlug(slug);
      if (postExists) {
        postData.slug = `${slug}-${uuid()}`;
      } else {
        postData.slug = slug;
      }

      // schema validation
      const schema = yup.object().shape({
        image: yup.string().nullable(),
        slug: yup.string().required(),
        title: yup.string().required(),
        content: yup.string().required(),
        view_count: yup
          .number()
          .integer()
          .required(),
        featured: yup.boolean().required(),
        user_id: yup
          .number()
          .integer()
          .required()
      });

      try {
        await schema.validate(postData);
      } catch (validationError) {
        return res.status(400).json(validationError);
      }

      // create post
      const createPost = await post.create(postData);
      return res.json(createPost);
    } catch (error) {
      return res.status(500).json({
        message: 'Server error'
      });
    }
  }
);

// PUT Method
// edit a post by id
// Private
// make admin only that can edit, better double slug loop
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  upload.single('image'),
  async (req, res) => {
    try {
      const { title, content, view_count, featured } = req.body;

      const postExists = await post.findByID(req.params.id);
      // return console.log(req.user);
      const postData = {
        title: title || postExists.title,
        slug: postExists.slug,
        content: content || postExists.content,
        view_count: view_count || postExists.view_count,
        featured: featured || postExists.featured,
        user_id: req.user.id
      };

      // if image is uploaded
      if (req.file) {
        postData.image = req.file.location;
      } else {
        postData.image = postExists.image;
      }

      // check if slug exists
      let slug = title.replace(/[\W_]+/g, '-').toLowerCase();
      const slugExists = await post.findBySlug(slug);
      if (slugExists) {
        if (slugExists.id !== postExists.id) {
          postData.slug = `${slug}-${uuid()}`;
        }
      } else {
        postData.slug = slug;
      }

      // schema validation
      const schema = yup.object().shape({
        image: yup.string().nullable(),
        title: yup.string().required(),
        slug: yup.string().required(),
        content: yup.string().required(),
        view_count: yup
          .number()
          .integer()
          .required(),
        featured: yup.boolean().required(),
        user_id: yup
          .number()
          .integer()
          .required()
      });
      try {
        await schema.validate(postData);
      } catch (validationError) {
        return res.status(400).json(validationError);
      }

      if (postExists) {
        const updatePost = await post.update(postExists.id, postData);
        const findUpdatedPost = await post.findByID(updatePost[0].id);
        return res.json(findUpdatedPost);
      } else {
        return res.status(404).json({
          message: 'no post found to be edited'
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Server error'
      });
    }
  }
);

// DELETE Method
// delete a post by ID
// Private
// make admin only that can delete
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  async (req, res) => {
    try {
      const postExists = await post.findByID(req.params.id);

      if (postExists) {
        const title = postExists.title;
        await post.delete(req.params.id);
        return res.json({
          message: `article '${title}' has been succesfully deleted`
        });
      } else {
        return res.status(404).json({
          message: 'no post found to be deleted'
        });
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Server error'
      });
    }
  }
);

module.exports = router;
