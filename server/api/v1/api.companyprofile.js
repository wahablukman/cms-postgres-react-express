const express = require('express');
const router = express.Router();
const verification = require('../../config/verification');
const passport = require('passport');
const companyProfile = require('../../queries/queries.companyprofile');
const upload = require('../../config/imageUpload');
let yup = require('yup');

// GET Method
// retrieve all company profile data
// public
//
router.get('/', async (req, res, next) => {
  try {
    const findCompanyProfile = await companyProfile.findCompanyProfile();
    if (findCompanyProfile) {
      return res.json(findCompanyProfile);
    } else {
      return res.json({
        message: 'its not yet created'
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// POST Method
// Create or update company profile
// Private - admin only
//
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  upload.single('logo'),
  async (req, res, next) => {
    const { name, email, facebook, instagram, youtube } = req.body;
    const companyProfileData = {
      name,
      email,
      facebook,
      instagram,
      youtube
    };

    // if image is uploaded
    if (req.file) {
      companyProfileData.logo = req.file.location;
    }

    // schema validation
    const schema = yup.object().shape({
      name: yup.string().required(),
      logo: yup.string().nullable(),
      email: yup
        .string()
        .email({ minDomainSegments: 2 })
        .required(),
      facebook: yup.string().nullable(),
      instagram: yup.string().nullable(),
      youtube: yup.string().nullable()
    });
    try {
      await schema.validate(companyProfileData);
    } catch (validationError) {
      return res.status(400).json(validationError);
    }

    try {
      const companyProfileExists = await companyProfile.findCompanyProfile();
      if (companyProfileExists) {
        const updateCompanyProfile = await companyProfile.update(
          companyProfileData
        );
        return res.json(updateCompanyProfile[0]);
      } else {
        const createCompanyProfile = await companyProfile.create(
          companyProfileData
        );
        return res.json(createCompanyProfile[0]);
      }
    } catch (error) {
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

module.exports = router;
