const express = require('express');
const router = express.Router();
const passport = require('passport');
const event = require('../../queries/queries.event');
const verification = require('../../config/verification');
const imageUpload = require('../../config/imageUpload');
const uuid = require('uuid/v4');
let yup = require('yup');

// GET Method
// get all events
// public
//
router.get('/', async (req, res) => {
  let queryPage = req.query.pg;
  let querySort = req.query.srt;
  queryPage ? (queryPage = queryPage) : (queryPage = 1);
  querySort ? (querySort = querySort) : (querySort = 'desc');

  try {
    const findEvents = await event.findAll(queryPage, querySort);
    return res.json(findEvents);
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// GET Method
// get all ACTIVE events
// public
//
router.get('/active', async (req, res) => {
  try {
    const findEvents = await event.findAllActive();
    return res.json(findEvents);
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// GET Method
// get event by ID
// Public
//
router.get('/:id', async (req, res) => {
  try {
    const findEvent = await event.findByID(req.params.id);
    if (findEvent) {
      return res.json(findEvent);
    } else {
      return res.status(404).json({
        message: 'There is no event found'
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// GET Method
// get event by ID
// Public
//
router.get('/slug/:slug', async (req, res) => {
  try {
    const findEvent = await event.findBySlug(req.params.slug);
    if (findEvent) {
      return res.json(findEvent);
    } else {
      return res.status(404).json({
        message: 'There is no event found'
      });
    }
  } catch (error) {
    return res.status(500).json({
      message: 'Server Error'
    });
  }
});

// POST Method
// create an event
// private & admin
//
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  imageUpload.single('image'),
  async (req, res) => {
    // create event data object
    let eventData = {
      title: req.body.title,
      place: req.body.place,
      description: req.body.description,
      start_at: req.body.start_at,
      finish_at: req.body.finish_at,
      accept_volunteer: req.body.accept_volunteer || true
    };

    try {
      // check slug
      let slug = req.body.title.replace(/[\W_]+/g, '-').toLowerCase();
      const slugExists = await event.findBySlug(slug);
      if (slugExists) {
        eventData.slug = `${slug}-${uuid()}`;
      } else {
        eventData.slug = slug;
      }

      // check if there is an image upload
      if (req.file) {
        eventData.image = req.file.location;
      } else {
        eventData.image = null;
      }

      // schema validation
      const schema = yup.object().shape({
        title: yup.string().required(),
        slug: yup.string().required(),
        image: yup.string().required(),
        place: yup.string().required(),
        description: yup.string().required(),
        start_at: yup.date().required(),
        finish_at: yup.date().required(),
        accept_volunteer: yup.boolean().required()
      });
      try {
        await schema.validate(eventData);
      } catch (validationError) {
        return res.status(400).json(validationError);
      }

      // insert event to db
      const createEvent = await event.create(eventData);
      return res.json(createEvent);
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

// PUT Method
// create an event
// private & admin
//
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  imageUpload.single('image'),
  async (req, res) => {
    try {
      // check if event exists
      const eventExists = await event.findByID(req.params.id);
      if (!eventExists) {
        return res.status(404).json({
          message: 'there is no event found to be edited'
        });
      }

      // create event data object
      let eventData = {
        id: eventExists.id,
        slug: eventExists.slug,
        title: req.body.title || eventExists.title,
        place: req.body.place || eventExists.place,
        description: req.body.description || eventExists.description,
        start_at: req.body.start_at || eventExists.start_at,
        finish_at: req.body.finish_at || eventExists.finish_at,
        accept_volunteer:
          req.body.accept_volunteer || eventExists.accept_volunteer
      };
      // return console.log(eventData);

      // check if slug exists
      let slug = req.body.title.replace(/[\W_]+/g, '-').toLowerCase();
      const slugExists = await event.findBySlug(slug);
      if (slugExists) {
        if (slugExists.id !== eventExists.id) {
          eventData.slug = `${slug}-${uuid()}`;
        }
      } else {
        eventData.slug = slug;
      }

      // check if there is an image upload
      if (req.file) {
        eventData.image = req.file.location;
      } else {
        eventData.image = eventExists.image;
      }

      // console.log(eventData);

      // schema validation
      const schema = yup.object().shape({
        title: yup.string().required(),
        slug: yup.string().required(),
        image: yup.string().required(),
        place: yup.string().required(),
        description: yup.string().required(),
        start_at: yup.date().required(),
        finish_at: yup.date().required(),
        accept_volunteer: yup.boolean().required()
      });
      try {
        await schema.validate(eventData);
      } catch (validationError) {
        return res.status(400).json(validationError);
      }

      // insert event to db
      const updateEvent = await event.update(eventExists.id, eventData);
      const findUpdatedEvent = await event.findByID(updateEvent[0].id);
      return res.json(findUpdatedEvent);
    } catch (error) {
      console.log(error);
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

// DELETE Method
// delete an event
// private && admin
//
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  verification.isAdmin,
  async (req, res) => {
    try {
      const eventExists = await event.findByID(req.params.id);

      // return error if event is not found
      if (!eventExists) {
        return res.status(404).json({
          message: 'There is no event found to be deleted'
        });
      }

      await event.delete(req.params.id);
      return res.json({
        message: 'Event is succesfully deleted'
      });
    } catch (error) {
      return res.status(500).json({
        message: 'Server Error'
      });
    }
  }
);

module.exports = router;
