module.exports = {
  isAdmin: (req, res, next) => {
    if (req.user) {
      if (req.user.role_id == 1) {
        next();
      }
    } else {
      return res.status(403).json({
        message: 'You are unauthorised make changes here'
      });
    }
  }
};
