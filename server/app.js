const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const cors = require('cors');
require('dotenv').config();

const app = express();

// console.log(moment());

// declare API routes
const users = require('./api/v1/api.users');
const post = require('./api/v1/api.post');
const event = require('./api/v1/api.event');
const companyProfile = require('./api/v1/api.companyprofile');

// cors white lists
const whitelist = ['http://localhost:3000/', 'http://example2.com'];
let corsOptions = {};
if (process.env.NODE_ENV !== 'development') {
  corsOptions.origin = (origin, callback) => {
    if (whitelist.indexOf(origin) !== -1) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  };
}

app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

// passport middleware
app.use(passport.initialize());
require('./config/passport')(passport);

app.get('/', async (req, res, next) => {
  res.json({
    message: 'test'
  });
});

// API routes
app.use('/api/v1/users', users);
app.use('/api/v1/post', post);
app.use('/api/v1/event', event);
app.use('/api/v1/company-profile', companyProfile);

module.exports = app;
