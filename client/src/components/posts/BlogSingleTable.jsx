import React from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';

const BlogSingleTable = ({ post, onClick }) => {
  return (
    <div className="row-content post">
      <div className="index" />
      <div className="title">
        <Link
          to={`/blog/${post.slug}`}
          title={post.title}
        >{`${post.title.substring(0, 65)}${
          post.title.length > 65 ? `...` : ''
        }`}</Link>
      </div>
      <div className="author">
        {post.user_first_name} {post.user_last_name}
      </div>
      <div className="last-updated">
        <Moment fromNow>{post.updated_at}</Moment>
      </div>
      <div className="btn-group action" role="group">
        <button
          id="post-action-dropdown"
          type="button"
          className="btn btn-outline-secondary dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          Action
        </button>
        <div className="dropdown-menu" aria-labelledby="post-action-dropdown">
          <Link to={`/blog/${post.slug}`}>Edit</Link>
          <hr />
          {/* eslint-disable-next-line */}
          <a onClick={onClick}>Delete</a>
        </div>
      </div>
    </div>
  );
};

export default BlogSingleTable;
