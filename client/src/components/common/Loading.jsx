import React from 'react';
import Lottie from 'lottie-react-web';
import animation from '../../assets/lottie/lottie-loading.json';

const Loading = () => {
  return (
    <div className="fullscreen-loading">
      <Lottie
        options={{
          animationData: animation,
          loop: true,
          autoplay: true
        }}
        speed={2}
      />
    </div>
  );
};

export default Loading;
