import React, { Component } from 'react';
import classnames from 'classnames';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

class WYSIWYG extends Component {
  state = {};
  render() {
    const {
      value,
      onChange,
      formats,
      modules,
      error,
      label,
      onBlur,
      errorValidation
    } = this.props;
    return (
      <div className="form-group">
        {errorValidation}
        <label>{label}</label>
        <ReactQuill
          value={value}
          modules={modules}
          formats={formats}
          onBlur={onBlur}
          onChange={onChange}
          className={classnames({
            'is-invalid': error
          })}
        />
        {error && <div className="invalid-feedback">{error}</div>}
      </div>
    );
  }
}

export default WYSIWYG;
