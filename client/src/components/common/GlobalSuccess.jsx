import React from 'react';

const GlobalSuccess = props => {
  return (
    <div className="alert alert-success" role="alert">
      <strong>
        <i className="fas fa-check" />
        {props.globalSuccess}
      </strong>
    </div>
  );
};

export default GlobalSuccess;
