import React from 'react';

const GlobalError = props => {
  return (
    <div className="alert alert-danger" role="alert">
      <strong>
        <i className="fas fa-times" />
        {props.globalError}
      </strong>
    </div>
  );
};

export default GlobalError;
