import React, { Component } from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as userActions from '../../redux/actions/userActions';
import * as companyProfileActions from '../../redux/actions/companyProfileActions';
import { ReactComponent as DashboardIcon } from '../../assets/icons/nav-dashboard.svg';
import { ReactComponent as BlogIcon } from '../../assets/icons/nav-blog.svg';
import { ReactComponent as EventIcon } from '../../assets/icons/nav-event.svg';
import { ReactComponent as SettingsIcon } from '../../assets/icons/nav-settings.svg';
import { ReactComponent as AccountIcon } from '../../assets/icons/nav-account.svg';
import { ReactComponent as LogoutIcon } from '../../assets/icons/nav-logout.svg';

class Nav extends Component {
  componentDidMount() {
    const { getUser, getCompanyProfile } = this.props;
    getUser();
    getCompanyProfile();
  }

  // Get navlink active class
  getExactNavLinkClass = path => {
    return this.props.location.pathname === path ? 'active' : '';
    // console.log(this.props);
  };
  // Get navlink active class
  getNavLinkClass = path => {
    return this.props.location.pathname.includes(path) ? 'active' : '';
    // console.log(this.props);
  };

  render() {
    const { user, companyProfile } = this.props;
    return (
      <div className="wrapper">
        <nav id="sidebar">
          <div className="sidebar-header company-logo">
            <img
              src={companyProfile.logo || ''}
              alt={`${companyProfile.name}'s logo`}
            />
          </div>

          <div className="author-info">
            <img
              className="avatar"
              src={
                user.avatar ||
                'http://cdn.onlinewebfonts.com/svg/img_276187.png'
              }
              alt={`${user.first_name}'s profile picturee`}
            />
            <h4 className="name">{`${user.first_name || ''} ${user.last_name ||
              ''}`}</h4>
            <p className="role">{user.role_name || ''}</p>
          </div>

          <ul className="nav-list list-unstyled components">
            <li className={this.getExactNavLinkClass('/')}>
              {/* <i className="fas fa-th-large" /> */}
              <div className="icon">
                <DashboardIcon />
              </div>
              <NavLink exact to="/">
                Dashboard
              </NavLink>
            </li>
            <li className={this.getNavLinkClass('/blog')}>
              {/* <i className="far fa-newspaper" /> */}
              <div className="icon">
                <BlogIcon />
              </div>
              <NavLink to="/blog">Blog</NavLink>
            </li>
            <li className={this.getNavLinkClass('/event')}>
              {/* <i className="far fa-calendar-alt" /> */}
              <div className="icon">
                <EventIcon />
              </div>
              <NavLink to="/event">Event</NavLink>
            </li>
            {/* <li className={this.getNavLinkClass('/enquiry')}>
              <i className="fas fa-envelope-open-text" />
              <NavLink to="/enquiry">Enquiry</NavLink>
            </li> */}
            <hr />
            <li className={this.getNavLinkClass('/settings')}>
              {/* <i className="fas fa-cogs" /> */}
              <div className="icon">
                <SettingsIcon />
              </div>
              <NavLink to="/settings">Settings</NavLink>
            </li>
            <li className={this.getNavLinkClass('/account')}>
              {/* <i className="fas fa-user" /> */}
              <div className="icon">
                <AccountIcon />
              </div>
              <NavLink to="/account">Account</NavLink>
            </li>
            <hr />
            <li>
              {/* <i className="fas fa-door-open" /> */}
              <div className="icon">
                <LogoutIcon />
              </div>
              <NavLink to="/logout">Logout</NavLink>
            </li>
            <hr />
            <a href="/client.com" className="btn btn-outline-info d-block">
              Go to your site
            </a>
          </ul>
        </nav>
      </div>
    );
  }
}

const NavWithRouter = withRouter(Nav);
const mapStateToProps = state => ({
  user: state.user,
  companyProfile: state.companyProfile
});

const mapDispatchToProps = {
  ...userActions,
  ...companyProfileActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NavWithRouter);
