import React, { Component } from 'react';
import Dropzone from 'react-dropzone';
import htmlParser from 'react-html-parser';

class FileUpload extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { onDrop, maxSize, uploadText, label, className } = this.props;
    return (
      <div className="form-group">
        <label>{label}</label>
        <div className="dropzone">
          <Dropzone onDrop={onDrop} className={className} maxSize={maxSize}>
            {htmlParser(uploadText)}
          </Dropzone>
        </div>
      </div>
    );
  }
}

export default FileUpload;
