import React from 'react';
import classnames from 'classnames';

const Pagination = ({
  pagination,
  handlePrevPagination,
  handleNextPagination
}) => {
  return (
    <div className="pagination-custom">
      <button
        onClick={handlePrevPagination}
        className={classnames('prev-button btn btn-primary', {
          disabled: pagination.current_page === 1,
          'pointer-events-none': pagination.current_page === 1
        })}
      >
        <i className="fas fa-angle-left" />
      </button>
      <button
        onClick={handleNextPagination}
        className={classnames('next-button btn btn-primary', {
          disabled: pagination.current_page === pagination.last_page,
          'pointer-events-none':
            pagination.current_page === pagination.last_page
        })}
      >
        <i className="fas fa-angle-right" />
      </button>
    </div>
  );
};

export default Pagination;
