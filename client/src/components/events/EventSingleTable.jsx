import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import Moment from 'react-moment';

const EventSingleTable = ({ event, onClick }) => {
  let displayDate;
  const sameMonth = moment(event.start_at).isSame(event.finish_at, 'month');
  const sameYear = moment(event.start_at).isSame(event.finish_at, 'year');
  if (sameYear) {
    if (sameMonth) {
      displayDate = `${moment(event.start_at).format('DD')} - ${moment(
        event.finish_at
      ).format('DD MMMM YYYY')}`;
    } else {
      displayDate = `${moment(event.start_at).format('DD MMMM')} - ${moment(
        event.finish_at
      ).format('DD MMMM YYYY')}`;
    }
  } else {
    displayDate = `${moment(event.start_at).format('DD MMMM YYYY')} - ${moment(
      event.finish_at
    ).format('DD MMMM YYYY')}`;
  }
  return (
    <div className="row-content event">
      <div className="index" />
      <div className="title">
        <Link
          to={`/event/${event.slug}`}
          title={event.title}
        >{`${event.title.substring(0, 65)}${
          event.title.length > 65 ? `...` : ''
        }`}</Link>
      </div>
      <div className="author">{displayDate}</div>
      <div className="last-updated">
        <Moment fromNow>{event.updated_at}</Moment>
      </div>
      <div className="btn-group action" role="group">
        <button
          id="post-action-dropdown"
          type="button"
          className="btn btn-outline-secondary dropdown-toggle"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          Action
        </button>
        <div className="dropdown-menu" aria-labelledby="post-action-dropdown">
          <Link to={`/event/${event.slug}`}>Edit</Link>
          <hr />
          {/* eslint-disable-next-line */}
          <a onClick={onClick}>Delete</a>
        </div>
      </div>
    </div>
  );
};

export default EventSingleTable;
