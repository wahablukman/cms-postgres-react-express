import React, { Component } from 'react';
import 'react-dates/initialize';
import './assets/css/main.scss';
import './assets/css/_lightTheme.scss';
import { Route } from 'react-router-dom';
import setAuthToken from './utils/setAuthToken';
import jwtDecode from 'jwt-decode';
import store from './redux/store';
import { setCurrentUser, logoutUser } from './redux/actions/authActions';

// Route components
import Home from './routes/Home';
import Blog from './routes/Blog';
import BlogSingle from './routes/BlogSingle';
import BlogCreate from './routes/BlogCreate';
import Event from './routes/Event';
import EventCreate from './routes/EventCreate';
import EventSingle from './routes/EventSingle';
import Enquiry from './routes/Enquiry';
import Login from './routes/Login';
import Settings from './routes/Settings';
import Account from './routes/Account';

// Shared components
import Nav from './components/common/Nav';
import PrivateRoute from './components/common/PrivateRoute';
import GlobalError from './components/common/GlobalError';
import GlobalSuccess from './components/common/GlobalSuccess';

import { connect } from 'react-redux';
import Loading from './components/common/Loading';

//check for token
if (localStorage.jwtToken) {
  // set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // decode token and get user info and exp
  const decoded = jwtDecode(localStorage.jwtToken);
  //set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // check for expired tokens
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    //logoutUser
    store.dispatch(logoutUser());
    window.location.href = '/login';
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { isLoading, auth, globalError, globalSuccess } = this.props;
    return (
      <>
        <div className="App">
          {auth.isAuthenticated ? <Nav /> : ''}
          <Route path="/login" exact component={Login} />
          <div className="content ">
            {globalSuccess !== '' ? (
              <GlobalSuccess globalSuccess={globalSuccess} />
            ) : (
              ''
            )}
            {globalError !== '' ? (
              <GlobalError globalError={globalError} />
            ) : (
              ''
            )}
            {isLoading && <Loading />}
            <PrivateRoute path="/" exact component={Home} />
            <PrivateRoute path="/blog" exact component={Blog} />
            <PrivateRoute path="/blog/:slug" exact component={BlogSingle} />
            <PrivateRoute path="/create/blog" exact component={BlogCreate} />
            <PrivateRoute path="/event" exact component={Event} />
            <PrivateRoute path="/event/:slug" exact component={EventSingle} />
            <PrivateRoute path="/create/event" exact component={EventCreate} />
            <PrivateRoute path="/enquiry" exact component={Enquiry} />
            <PrivateRoute path="/settings" exact component={Settings} />
            <PrivateRoute path="/account" exact component={Account} />
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: state.isLoading,
  auth: state.auth,
  globalError: state.globalError,
  globalSuccess: state.globalSuccess
});

export default connect(mapStateToProps)(App);
