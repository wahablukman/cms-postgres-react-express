import {
  GET_ALL_POSTS,
  GET_A_POST,
  UPDATE_A_POST,
  CREATE_A_POST,
  DELETE_A_POST
} from '../actions/types';

const initialState = {
  posts: [],
  pagination: {
    total: 1,
    current_page: 1,
    last_page: 1,
    per_page: 1
  },
  post: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_POSTS:
      return {
        ...state,
        posts: action.payload.data,
        pagination: {
          total: parseInt(action.payload.total),
          per_page: parseInt(action.payload.per_page),
          current_page: parseInt(action.payload.current_page),
          last_page: parseInt(action.payload.last_page)
        }
      };
    case GET_A_POST:
      return {
        ...state,
        post: action.payload
      };
    case UPDATE_A_POST:
      return {
        ...state,
        posts: state.posts.map(post =>
          post.id === action.payload.id ? action.payload : post
        ),
        post: action.payload
      };
    case CREATE_A_POST:
      return {
        ...state,
        posts: [action.payload, state.posts]
      };
    case DELETE_A_POST:
      return {
        ...state,
        posts: state.posts.filter(post => post.id !== action.payload)
      };
    default:
      return state;
  }
};
