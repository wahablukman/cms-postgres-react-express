import { combineReducers } from 'redux';
import authReducer from './authReducer';
import postReducer from './postReducer';
import eventReducer from './eventReducer';
import loadingReducer from './loadingReducer';
import userReducer from './userReducer';
import globalError from './errorReducer';
import globalSuccess from './successReducer';
import companyProfileReducer from './companyProfileReducer';

export default combineReducers({
  auth: authReducer,
  user: userReducer,
  posts: postReducer,
  event: eventReducer,
  companyProfile: companyProfileReducer,
  globalError: globalError,
  globalSuccess: globalSuccess,
  isLoading: loadingReducer
});
