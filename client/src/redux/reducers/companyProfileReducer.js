import {
  GET_COMPANY_PROFILE,
  CREATE_OR_UPDATE_COMPANY_PROFILE
} from '../actions/types';

const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_COMPANY_PROFILE:
      return action.payload;
    case CREATE_OR_UPDATE_COMPANY_PROFILE:
      return action.payload;
    default:
      return state;
  }
};
