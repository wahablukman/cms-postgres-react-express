import { GET_GLOBAL_SUCCESS, CLEAR_GLOBAL_SUCCESS } from '../actions/types';
const initialState = '';

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_GLOBAL_SUCCESS:
      return action.payload;
    case CLEAR_GLOBAL_SUCCESS:
      return '';
    default:
      return state;
  }
};
