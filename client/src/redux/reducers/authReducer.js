import { SET_CURRENT_AUTH } from '../actions/types';
import isEmpty from '../../utils/isEmpty';

const initialState = {
  isAuthenticated: false,
  auth: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_AUTH:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        auth: action.payload
      };
    default:
      return state;
  }
};
