import {
  GET_ALL_EVENTS,
  GET_AN_EVENT,
  CREATE_AN_EVENT,
  UPDATE_AN_EVENT,
  DELETE_AN_EVENT
} from '../actions/types';

const initialState = {
  events: [],
  pagination: {
    total: 1,
    current_page: 1,
    last_page: 1,
    per_page: 1
  },
  event: {}
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_ALL_EVENTS:
      return {
        ...state,
        events: action.payload.data,
        pagination: {
          total: parseInt(action.payload.total) || 1,
          per_page: parseInt(action.payload.per_page) || 1,
          current_page: parseInt(action.payload.current_page) || 1,
          last_page: parseInt(action.payload.last_page) || 1
        }
      };
    case GET_AN_EVENT:
      return {
        ...state,
        event: action.payload
      };
    case CREATE_AN_EVENT:
      return {
        ...state,
        events: [action.payload, state.events]
      };
    case UPDATE_AN_EVENT:
      return {
        ...state,
        events: state.events.map(event =>
          event.id === action.payload.id ? action.payload : event
        ),
        event: action.payload
      };
    case DELETE_AN_EVENT:
      return {
        ...state,
        events: state.events.filter(event => event.id !== action.payload)
      };
    default:
      return state;
  }
};
