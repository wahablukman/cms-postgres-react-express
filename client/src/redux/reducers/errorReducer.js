import { GET_GLOBAL_ERROR, CLEAR_GLOBAL_ERROR } from '../actions/types';
const initialState = '';

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_GLOBAL_ERROR:
      return action.payload;
    case CLEAR_GLOBAL_ERROR:
      return '';
    default:
      return state;
  }
};
