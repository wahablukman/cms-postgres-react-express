import axios from 'axios';
import jwtDecode from 'jwt-decode';
import setAuthToken from '../../utils/setAuthToken';
import { isLoading } from './loadingActions';
import { getGlobalSuccess } from './successActions';

import { SET_CURRENT_AUTH } from './types';

export const loginUser = (userData, history) => async dispatch => {
  dispatch(isLoading(true));
  const user = await axios.post('/api/v1/users/login', userData);
  //save to local storage
  const { token } = user.data;
  //set token to local storage
  localStorage.setItem('jwtToken', token);
  //set token to auth header
  setAuthToken(token);
  //decode token to get user data
  const decoded = jwtDecode(token);
  //set current user
  dispatch(setCurrentUser(decoded));
  history.push('/');
  dispatch(isLoading(false));
  return dispatch(getGlobalSuccess('Login succesful !'));
};

//set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_AUTH,
    payload: decoded
  };
};

// log user out
export const logoutUser = () => dispatch => {
  // remove token from localStorage
  localStorage.removeItem('jwtToken');
  // remove auth header for future requests
  setAuthToken(false);
  // set current user to {} which will also set isAuthenticated to false
  return dispatch(setCurrentUser({}));
};
