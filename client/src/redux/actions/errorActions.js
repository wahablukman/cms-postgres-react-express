import { GET_GLOBAL_ERROR, CLEAR_GLOBAL_ERROR } from './types';

export const getGlobalError = globalErrorMessage => dispatch => {
  dispatch({
    type: GET_GLOBAL_ERROR,
    payload: globalErrorMessage
  });
  setTimeout(() => {
    dispatch({
      type: CLEAR_GLOBAL_ERROR
    });
  }, 4000);
};
