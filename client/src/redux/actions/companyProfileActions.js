import axios from 'axios';
import { isLoading } from './loadingActions';
import { getGlobalError } from './errorActions';
import { getGlobalSuccess } from './successActions';
import { GET_COMPANY_PROFILE, CREATE_OR_UPDATE_COMPANY_PROFILE } from './types';

export const getCompanyProfile = () => async dispatch => {
  const companyProfile = await axios.get('/api/v1/company-profile');
  if (companyProfile.data) {
    return dispatch({
      type: GET_COMPANY_PROFILE,
      payload: companyProfile.data
    });
  }
};

export const updateCompanyProfile = companyProfileData => async dispatch => {
  dispatch(isLoading(true));
  try {
    const companyProfile = await axios.post(
      '/api/v1/company-profile',
      companyProfileData
    );
    dispatch({
      type: CREATE_OR_UPDATE_COMPANY_PROFILE,
      payload: companyProfile.data
    });
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('Company profile updated!'));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message));
  }
};
