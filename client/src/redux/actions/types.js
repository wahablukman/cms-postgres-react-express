// error
export const GET_GLOBAL_ERROR = 'GET_GLOBAL_ERROR';
export const CLEAR_GLOBAL_ERROR = 'CLEAR_GLOBAL_ERROR';

// success
export const GET_GLOBAL_SUCCESS = 'GET_GLOBAL_SUCCESS';
export const CLEAR_GLOBAL_SUCCESS = 'CLEAR_GLOBAL_SUCCESS';

// company profile
export const GET_COMPANY_PROFILE = 'GET_COMPANY_PROFILE';
export const CREATE_OR_UPDATE_COMPANY_PROFILE =
  'CREATE_OR_UPDATE_COMPANY_PROFILE';

// blog
export const GET_ALL_POSTS = 'GET_ALL_POSTS';
export const GET_A_POST = 'GET_A_POST';
export const CREATE_A_POST = 'CREATE_A_POST';
export const UPDATE_A_POST = 'UPDATE_A_POST';
export const DELETE_A_POST = 'DELETE_A_POST';

// event
export const GET_ALL_EVENTS = 'GET_ALL_EVENTS';
export const GET_ALL_ACTIVE_EVENTS = 'GET_ALL_ACTIVE_EVENTS';
export const GET_AN_EVENT = 'GET_AN_EVENT';
export const CREATE_AN_EVENT = 'CREATE_AN_EVENT';
export const UPDATE_AN_EVENT = 'UPDATE_AN_EVENT';
export const DELETE_AN_EVENT = 'DELETE_AN_EVENT';

// loading
export const IS_LOADING = 'IS_LOADING';

// auth
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SET_CURRENT_AUTH = 'SET_CURRENT_AUTH';
