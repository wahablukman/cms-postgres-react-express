import axios from 'axios';
import { isLoading } from './loadingActions';
import { getGlobalSuccess } from './successActions';
import { getGlobalError } from './errorActions';
import { SET_CURRENT_USER } from './types';

export const getUser = () => async dispatch => {
  const user = await axios.get(`/api/v1/users/user`);
  if (user.data) {
    dispatch({
      type: SET_CURRENT_USER,
      payload: user.data
    });
  } else {
    return console.log('no found user');
  }
};

export const updateUser = userData => async dispatch => {
  dispatch(isLoading(true));
  try {
    const updateUser = await axios.put('/api/v1/users/user', userData);
    dispatch({
      type: SET_CURRENT_USER,
      payload: updateUser.data
    });
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('User profile update success !'));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message));
  }
};
