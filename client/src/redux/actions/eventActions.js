import axios from 'axios';
import { isLoading } from './loadingActions';
import { getGlobalSuccess } from './successActions';
import { getGlobalError } from './errorActions';
import {
  GET_ALL_EVENTS,
  GET_AN_EVENT,
  CREATE_AN_EVENT,
  UPDATE_AN_EVENT,
  DELETE_AN_EVENT
} from './types';

export const getAllEvents = queryString => async dispatch => {
  dispatch(isLoading(true));
  try {
    const events = await axios.get(`/api/v1/event/${queryString}`);
    dispatch({
      type: GET_ALL_EVENTS,
      payload: events.data
    });
    return dispatch(isLoading(false));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message || error));
  }
};

export const getEventBySlug = slug => async dispatch => {
  dispatch(isLoading(true));
  try {
    const event = await axios.get(`/api/v1/event/slug/${slug}`);
    dispatch({
      type: GET_AN_EVENT,
      payload: event.data
    });
    return dispatch(isLoading(false));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message));
  }
};

export const createEvent = (eventData, history) => async dispatch => {
  dispatch(isLoading(true));
  try {
    const createEvent = await axios.post('/api/v1/event', eventData);
    dispatch({
      type: CREATE_AN_EVENT,
      payload: createEvent.data
    });
    history.push('/event');
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('Event created succesfully'));
  } catch (error) {
    history.push('/event');
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message));
  }
};

export const updateEvent = (eventData, eventID, history) => async dispatch => {
  dispatch(isLoading(true));
  try {
    const updateEvent = await axios.put(`/api/v1/event/${eventID}`, eventData);
    dispatch({
      type: UPDATE_AN_EVENT,
      payload: updateEvent.data
    });
    history.push('/event');
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('Event updated'));
  } catch (error) {
    history.push('/event');
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message));
  }
};

export const deleteEvent = eventID => async dispatch => {
  dispatch(isLoading(true));
  try {
    await axios.delete(`/api/v1/event/${eventID}`);
    dispatch({
      type: DELETE_AN_EVENT,
      payload: eventID
    });
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('Event deleted'));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message));
  }
};
