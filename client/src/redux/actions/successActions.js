import { GET_GLOBAL_SUCCESS, CLEAR_GLOBAL_SUCCESS } from './types';

export const getGlobalSuccess = globalSuccessMessage => dispatch => {
  dispatch({
    type: GET_GLOBAL_SUCCESS,
    payload: globalSuccessMessage
  });
  setTimeout(() => {
    dispatch({
      type: CLEAR_GLOBAL_SUCCESS
    });
  }, 4000);
};
