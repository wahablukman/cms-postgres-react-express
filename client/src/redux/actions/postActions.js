import axios from 'axios';

import {
  GET_ALL_POSTS,
  GET_A_POST,
  UPDATE_A_POST,
  CREATE_A_POST,
  DELETE_A_POST
} from './types';
import { isLoading } from './loadingActions';
import { getGlobalSuccess } from './successActions';
import { getGlobalError } from './errorActions';

export const getAllPosts = queryString => async dispatch => {
  dispatch(isLoading(true));
  try {
    const posts = await axios.get(`/api/v1/post${queryString}`);
    dispatch({
      type: GET_ALL_POSTS,
      payload: posts.data
    });
    return dispatch(isLoading(false));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(getGlobalError(error.response.data.message || error));
  }
};

export const getPostBySlug = slug => async dispatch => {
  dispatch(isLoading(true));
  const post = await axios.get(`/api/v1/post/slug/${slug}`);
  if (post.data) {
    dispatch({
      type: GET_A_POST,
      payload: post.data
    });
    return dispatch(isLoading(false));
  } else {
    console.log('cannot find a post with that slug');
  }
};

export const updatePost = (postData, postID, history) => async dispatch => {
  dispatch(isLoading(true));
  try {
    const updatePost = await axios.put(`/api/v1/post/${postID}`, postData);
    dispatch({
      type: UPDATE_A_POST,
      payload: updatePost.data
    });
    history.push('/blog');
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('Your blog is succesfully updated'));
  } catch (error) {
    history.push('/blog');
    dispatch(isLoading(false));
    return dispatch(
      getGlobalError(`Failed to update - ${error.response.data.message}`)
    );
  }
};

export const createPost = (postData, history) => async dispatch => {
  dispatch(isLoading(true));
  try {
    const createPost = await axios.post('/api/v1/post', postData);
    dispatch({
      type: CREATE_A_POST,
      payload: createPost.data
    });
    history.push('/blog');
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('Your blog is succesfully created'));
  } catch (error) {
    history.push('/blog');
    dispatch(isLoading(false));
    return dispatch(
      getGlobalError(`Failed to update - ${error.response.data.message}`)
    );
  }
};

export const deletePost = postID => async dispatch => {
  dispatch(isLoading(true));
  try {
    await axios.delete(`/api/v1/post/${postID}`);
    dispatch({
      type: DELETE_A_POST,
      payload: postID
    });
    dispatch(isLoading(false));
    return dispatch(getGlobalSuccess('The blog was succesfully removed'));
  } catch (error) {
    dispatch(isLoading(false));
    return dispatch(
      getGlobalError(
        `Failed to remove the blog - ${error.response.data.message}`
      )
    );
  }
};
