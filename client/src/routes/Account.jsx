import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { withFormik, Field } from 'formik';
import FileUpload from '../components/common/FileUpload';
import * as userActions from '../redux/actions/userActions';

let yup = require('yup');

class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      avatar: '',
      newAvatar: {}
    };
  }

  async componentDidMount() {
    const { getUser } = this.props;
    await getUser();
    await this.setState({
      avatar: this.props.user.avatar
    });
  }

  convertImage = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          avatar: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  handleDrop = (file, rejectedFile) => {
    const image = file[0];
    this.convertImage(image);
    this.setState(
      {
        newAvatar: image
      },
      () => {
        this.props.setValues({
          ...this.props.values,
          avatar: this.state.newAvatar
        });
      }
    );
  };

  handleSubmit = e => {
    e.preventDefault();
    const { first_name, last_name, avatar } = this.props.values;
    const { updateUser } = this.props;
    let userData = new FormData();
    userData.append('first_name', first_name);
    userData.append('last_name', last_name);
    userData.append('avatar', avatar);
    updateUser(userData);
  };

  render() {
    const { values } = this.props;
    return (
      <div>
        <Helmet>
          <title>Settings</title>
        </Helmet>
        <h1 className="route-title">Settings</h1>
        <br />
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-4">
              <div className="form-group">
                <label>First Name</label>
                <Field
                  className="form-control"
                  type="text"
                  name="first_name"
                  value={values.first_name}
                />
              </div>
              <div className="form-group">
                <label>Last Name</label>
                <Field
                  className="form-control"
                  type="text"
                  name="last_name"
                  value={values.last_name}
                />
              </div>
            </div>
            <div className="col-md-3">
              <FileUpload
                label="Upload Profile Picture"
                onDrop={this.handleDrop}
                maxSize={1000000}
                className="dropzone-filedrop image-prev avatar"
                uploadText={`<img src=${this.state.avatar}>`}
              />
            </div>
          </div>

          <button type="submit" className="btn btn-primary">
            Save changes
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.user
});

const mapDispatchToProps = {
  ...userActions
};

const AccountFormik = withFormik({
  enableReinitialize: true,
  mapPropsToValues(props) {
    return {
      first_name: props.user.first_name || '',
      last_name: props.user.last_name || ''
    };
  },
  validationSchema: yup.object().shape({
    first_name: yup.string().required(),
    last_name: yup.string().required()
  })
})(Account);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AccountFormik);
