import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from '../redux/actions/postActions';
import Helmet from 'react-helmet';
import { withFormik, Field } from 'formik';
import WYSIWYG from '../components/common/WYSIWYG';
import FileUpload from '../components/common/FileUpload';
import isEmpty from '../utils/isEmpty';
let yup = require('yup');
// import Yup from 'yup';

class BlogCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      featuredImage: '',
      newImage: {}
    };
  }

  componentDidMount() {}

  convertImage = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          featuredImage: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  handleDrop = (file, rejectedFile) => {
    const image = file[0];
    this.convertImage(image);
    this.setState(
      {
        newImage: image
      },
      () => {
        this.props.setValues({
          ...this.props.values,
          image: this.state.newImage
        });
      }
    );
  };

  handleWYSIWYGChange = value => {
    this.props.setValues({
      ...this.props.values,
      content: value
    });
  };

  handleWYSIWYGBlur = () => {
    this.props.setTouched({
      ...this.props.touched,
      content: true
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    let { title, content, image, slug } = this.props.values;
    const { createPost, history } = this.props;
    let postData = new FormData();
    postData.append('slug', slug);
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image);
    // console.log(this.props.values);
    createPost(postData, history);
  };

  render() {
    const { values, history, touched, errors } = this.props;

    return (
      <form onSubmit={this.handleSubmit}>
        <Helmet>
          <title>Create a new blog</title>
        </Helmet>
        <h1 className="route-title">Create a new blog</h1>
        <div className="row">
          <div className="col-md-9">
            <FileUpload
              label="Upload featured image"
              onDrop={this.handleDrop}
              maxSize={1000000}
              uploadText={`<i class="fas fa-cloud-upload-alt"></i><p>Click / drag your image here</p>`}
              className="dropzone-filedrop"
            />
            <div className="form-group">
              {touched.title && errors.title && (
                <div className="invalid-feedback ">{errors.title}</div>
              )}
              <label>Title</label>
              <Field
                className="form-control"
                type="text"
                name="title"
                value={values.title}
              />
            </div>

            <WYSIWYG
              onChange={this.handleWYSIWYGChange}
              value={values.content}
              modules={BlogCreate.modules}
              label="Content"
              onBlur={this.handleWYSIWYGBlur}
              errorValidation={
                touched.content &&
                errors.content && (
                  <div className="invalid-feedback">{errors.content}</div>
                )
              }
            />
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label>Featured Image</label>
              {this.state.featuredImage ? (
                <div className="preview-image">
                  <img
                    alt={`blog's featured pict`}
                    src={this.state.featuredImage}
                  />
                </div>
              ) : null}
            </div>
          </div>
        </div>
        <button
          disabled={touched.title && isEmpty(errors) ? '' : 'disabled'}
          type="submit"
          className="btn btn-primary"
        >
          Create post
        </button>
        <button
          onClick={
            (this.goBack = () => {
              history.push('/blog');
            })
          }
          className="btn btn-outline-info ml-2"
        >
          cancel
        </button>
      </form>
    );
  }
}

BlogCreate.modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }],
    [{ align: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ color: [] }, { background: [] }],
    ['link', 'image'],
    ['code-block']
  ]
};

const mapDispatchToProps = {
  ...postActions
};

const BlogCreateFormik = withFormik({
  enableReinitialize: true,
  mapPropsToValues() {
    return {
      image: '',
      title: '',
      content: ''
    };
  },
  validationSchema: yup.object().shape({
    // image: yup.string().required('Post image is needed'),
    title: yup.string().required('Title is required'),
    content: yup.string(10).required('Content is required to be filled')
  })
})(BlogCreate);

const mapStateToProps = state => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlogCreateFormik);
