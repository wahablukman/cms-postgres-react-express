import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import Pagination from '../components/common/Pagination';
import { connect } from 'react-redux';
import * as eventActions from '../redux/actions/eventActions';
import EventSingleTable from '../components/events/EventSingleTable';
import isEmpty from '../utils/isEmpty';

class Event extends Component {
  constructor(props) {
    super(props);
    this.state = {
      events: [],
      pagination: { total: 1, per_page: 1, current_page: 1, last_page: 1 }
    };
  }

  componentDidMount() {
    const { getAllEvents } = this.props;
    let queryParam = this.props.history.location.search || '';
    getAllEvents(queryParam);
  }

  handlePrevPagination = () => {
    const { history, getAllEvents } = this.props;
    const { pagination } = this.state;
    this.setState(
      {
        pagination: {
          ...this.state.pagination,
          current_page: pagination.current_page--
        }
      },
      () => {
        history.push({
          pathname: '/blog',
          search: `?pg=${pagination.current_page}`
        });
        getAllEvents(this.props.history.location.search);
      }
    );
  };

  handleNextPagination = () => {
    const { history, getAllEvents } = this.props;
    const { pagination } = this.state;
    this.setState(
      {
        pagination: {
          ...this.state.pagination,
          current_page: pagination.current_page++
        }
      },
      () => {
        history.push({
          pathname: '/blog',
          search: `?pg=${pagination.current_page}`
        });
        getAllEvents(this.props.history.location.search);
      }
    );
  };

  componentDidUpdate(prevProps) {
    if (
      prevProps.pagination !== this.props.pagination ||
      prevProps.events !== this.props.events
    ) {
      this.setState({
        events: this.props.events,
        pagination: this.props.pagination
      });
    }
  }

  handleDelete = eventID => {
    const { deleteEvent } = this.props;
    deleteEvent(eventID);
  };

  render() {
    let { pagination, events } = this.state;
    if (!isEmpty(events)) {
      events = events.map((event, index) => {
        return (
          <EventSingleTable
            event={event}
            onClick={() => this.handleDelete(event.id)}
            key={index}
          />
        );
      });
    } else {
      events = (
        <div className="empty">
          You have not created an event yet.&nbsp;
          <Link to="/create/event">Click here</Link> &nbsp;to create an event
        </div>
      );
    }

    return (
      <div>
        <Helmet>
          <title>Events</title>
        </Helmet>
        <h1 className="route-title">Event</h1>
        <Link
          to="/create/event"
          title="Create new event"
          className="btn btn-primary mb-2 add-new"
        >
          <i className="fas fa-plus" />
        </Link>
        <Pagination
          pagination={pagination}
          handleNextPagination={this.handleNextPagination}
          handlePrevPagination={this.handlePrevPagination}
        />
        <div className="row-header-wrapper">
          <div className="row-header index">No.</div>
          <div className="row-header title">Title</div>
          <div className="row-header author">Date</div>
          <div className="row-header last-updated">Last Updated</div>
        </div>
        <div className="row-content-wrapper">{events}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  events: state.event.events,
  pagination: state.event.pagination
});

const mapDispatchToProps = {
  ...eventActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Event);
