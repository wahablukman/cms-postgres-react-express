import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from '../redux/actions/postActions';
import Helmet from 'react-helmet';
import { withFormik, Field } from 'formik';
import WYSIWYG from '../components/common/WYSIWYG';
import FileUpload from '../components/common/FileUpload';
import isEmpty from '../utils/isEmpty';
let yup = require('yup');
// import Yup from 'yup';

class BlogSingle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      featuredImage: '',
      newImage: {}
    };
  }

  async componentDidMount() {
    const slug = this.props.match.params.slug;
    const { getPostBySlug } = this.props;
    await getPostBySlug(slug);
    await this.setState({
      featuredImage: this.props.post.image
    });
  }

  convertImage = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          featuredImage: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  handleDrop = (file, rejectedFile) => {
    const image = file[0];
    this.convertImage(image);
    this.setState(
      {
        newImage: image
      },
      () => {
        this.props.setValues({
          ...this.props.values,
          image: this.state.newImage
        });
      }
    );
  };

  componentDidUpdate(prevProps) {
    if (prevProps.post.image !== this.props.post.image) {
      this.setState({
        featuredImage: this.props.post.image
      });
    }
  }

  handleWYSIWYGChange = value => {
    this.props.setValues({
      ...this.props.values,
      content: value
    });
  };

  handleWYSIWYGBlur = () => {
    this.props.setTouched({
      ...this.props.touched,
      content: true
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    let {
      title,
      content,
      image,
      id,
      slug,
      user_id,
      view_count
    } = this.props.values;
    const { updatePost, history } = this.props;
    let postData = new FormData();
    postData.append('id', id);
    postData.append('slug', slug);
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image);
    postData.append('user_id', user_id);
    postData.append('view_count', view_count);
    // console.log(this.props.values);
    updatePost(postData, id, history);
  };

  render() {
    const { values, post, history, touched, errors } = this.props;
    return (
      <form onSubmit={this.handleSubmit}>
        <Helmet>
          <title>{post.title}</title>
        </Helmet>
        <h1 className="route-title">Edit Post</h1>
        <div className="row">
          <div className="col-md-9">
            <FileUpload
              label="Upload featured image"
              onDrop={this.handleDrop}
              maxSize={1000000}
              uploadText={`<i class="fas fa-cloud-upload-alt"></i><p>Click / drag your image here</p>`}
              className="dropzone-filedrop"
            />
            <div className="form-group">
              {touched.title && errors.title && (
                <div className="invalid-feedback ">{errors.title}</div>
              )}
              <label>Title</label>
              <Field
                className="form-control"
                type="text"
                name="title"
                value={values.title}
              />
            </div>
            <WYSIWYG
              onChange={this.handleWYSIWYGChange}
              value={values.content}
              modules={BlogSingle.modules}
              label="Content"
              errorValidation={
                touched.content &&
                errors.content && (
                  <div className="invalid-feedback">{errors.content}</div>
                )
              }
            />
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label>Featured Image</label>
              {this.state.featuredImage ? (
                <div className="preview-image">
                  <img
                    alt={`${post.title}'s featured pict`}
                    src={this.state.featuredImage}
                  />
                </div>
              ) : null}
            </div>
          </div>
        </div>
        <button
          disabled={isEmpty(errors) ? '' : 'disabled'}
          type="submit"
          className="btn btn-primary"
        >
          Edit post
        </button>
        <button
          onClick={
            (this.goBack = () => {
              history.push('/blog');
            })
          }
          className="btn btn-outline-info ml-2"
        >
          cancel
        </button>
      </form>
    );
  }
}

BlogSingle.modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }],
    [{ align: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ color: [] }, { background: [] }],
    ['link', 'image'],
    ['code-block']
  ]
};

const mapStateToProps = state => ({
  post: state.posts.post
});

const mapDispatchToProps = {
  ...postActions
};

const BlogSingleFormik = withFormik({
  enableReinitialize: true,
  mapPropsToValues(props) {
    return {
      id: props.post.id || 0,
      featured: props.post.featured || false,
      image: props.post.image || '',
      slug: props.post.slug || '',
      title: props.post.title || '',
      view_count: props.post.view_count || 0,
      content: props.post.content || '',
      user_id: props.post.user_id || 0
    };
  },
  validationSchema: yup.object().shape({
    // image: yup.string().required('Post image is needed'),
    title: yup.string().required('Title is required'),
    content: yup.string(10).required('Content is required to be filled')
  })
})(BlogSingle);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlogSingleFormik);
