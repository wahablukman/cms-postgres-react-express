import React, { Component } from 'react';
import Helmet from 'react-helmet';

class Enquiry extends Component {
  state = {};
  render() {
    return (
      <div>
        <Helmet>
          <title>Enquiry</title>
        </Helmet>
        <h1 className="route-title">Enquiry</h1>
      </div>
    );
  }
}

export default Enquiry;
