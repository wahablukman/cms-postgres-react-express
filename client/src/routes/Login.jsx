import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as authActions from '../redux/actions/authActions';
import * as companyProfileActions from '../redux/actions/companyProfileActions';
import Helmet from 'react-helmet';
import { withFormik, Field, Form } from 'formik';

class Login extends Component {
  state = {};

  componentDidMount() {
    const { auth, history, getCompanyProfile } = this.props;
    if (auth.isAuthenticated) {
      history.push('/');
    }
    getCompanyProfile();
  }

  render() {
    const { isSubmitting, values, companyProfile } = this.props;
    return (
      <div className="login-page">
        <Helmet>
          <title>Login</title>
        </Helmet>
        <div className="company-logo">
          <img
            src={companyProfile.logo || ''}
            alt={`${companyProfile.name}'s logo`}
          />
        </div>
        <Form className="col-md-3">
          <div className="form-group">
            <label>E-mail</label>
            <Field
              className="form-control"
              type="email"
              name="email"
              value={values.email}
            />
          </div>
          <div className="form-group">
            <label>Password</label>
            <Field
              className="form-control"
              type="password"
              name="password"
              value={values.password}
            />
          </div>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={isSubmitting}
          >
            Login
          </button>
        </Form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  companyProfile: state.companyProfile,
  auth: state.auth
});

const mapDispatchToProps = {
  ...authActions,
  ...companyProfileActions
};

const LoginFormik = withFormik({
  mapPropsToValues({ email, password }) {
    return { email: email || '', password: password || '' };
  },
  handleSubmit(values, { props, setSubmitting }) {
    const { loginUser, history } = props;
    loginUser(values, history);
    setSubmitting(false);
  }
})(Login);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginFormik);
