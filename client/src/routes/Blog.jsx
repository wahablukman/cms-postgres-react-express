import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as postActions from '../redux/actions/postActions';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import BlogSingleTable from '../components/posts/BlogSingleTable';
import Pagination from '../components/common/Pagination';
import isEmpty from '../utils/isEmpty';

class Blog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      pagination: { total: 1, per_page: 1, current_page: 1, last_page: 1 }
    };
  }

  componentDidMount() {
    const { getAllPosts } = this.props;
    let queryParam = this.props.history.location.search || '';
    getAllPosts(queryParam);
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.posts !== this.props.posts ||
      prevProps.pagination !== this.props.pagination
    ) {
      this.setState({
        posts: this.props.posts,
        pagination: this.props.pagination
      });
    }
  }

  handlePrevPagination = () => {
    const { history, getAllPosts } = this.props;
    const { pagination } = this.state;
    this.setState(
      {
        pagination: {
          ...this.state.pagination,
          current_page: pagination.current_page--
        }
      },
      () => {
        history.push({
          pathname: '/blog',
          search: `?pg=${pagination.current_page}`
        });
        getAllPosts(this.props.history.location.search);
      }
    );
  };

  handleNextPagination = () => {
    const { history, getAllPosts } = this.props;
    const { pagination } = this.state;
    this.setState(
      {
        pagination: {
          ...this.state.pagination,
          current_page: pagination.current_page++
        }
      },
      () => {
        history.push({
          pathname: '/blog',
          search: `?pg=${pagination.current_page}`
        });
        getAllPosts(this.props.history.location.search);
      }
    );
  };

  handleDelete = postID => {
    const { deletePost } = this.props;
    deletePost(postID);
  };

  render() {
    let { posts, pagination } = this.state;
    if (!isEmpty(posts)) {
      posts = posts.map((post, index) => {
        return (
          <BlogSingleTable
            post={post}
            key={index}
            onClick={() => this.handleDelete(post.id)}
          />
        );
      });
    } else {
      posts = (
        <div className="empty">
          You have not created a blog yet.&nbsp;
          <Link to="/create/blog">Click here</Link> &nbsp;to create a blog
        </div>
      );
    }

    return (
      <div className="posts-parent">
        <Helmet>
          <title>Blog</title>
        </Helmet>
        {/* possible stand alone component */}
        <h1 className="route-title">Blogs</h1>
        <Link
          to="/create/blog"
          title="Create new blog"
          className="btn btn-primary mb-2 add-new"
        >
          <i className="fas fa-plus" />
        </Link>
        <Pagination
          pagination={pagination}
          handleNextPagination={this.handleNextPagination}
          handlePrevPagination={this.handlePrevPagination}
        />
        <div className="row-header-wrapper">
          <div className="row-header index">No.</div>
          <div className="row-header title">Title</div>
          <div className="row-header author">Author</div>
          <div className="row-header last-updated">Last Updated</div>
        </div>
        <div className="row-content-wrapper">{posts}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  posts: state.posts.posts,
  pagination: state.posts.pagination
});

const mapDispatchToProps = {
  ...postActions
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Blog);
