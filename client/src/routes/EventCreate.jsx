import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as eventActions from '../redux/actions/eventActions';
import Helmet from 'react-helmet';
import { withFormik, Field } from 'formik';
import WYSIWYG from '../components/common/WYSIWYG';
import FileUpload from '../components/common/FileUpload';
import isEmpty from '../utils/isEmpty';
import moment from 'moment';
import { DateRangePicker } from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';

let yup = require('yup');
// import Yup from 'yup';

class EventCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      featuredImage: '',
      newImage: {},
      startDate: null,
      endDate: null,
      focusedInput: null
    };
  }

  componentDidMount() {
    // this.setState({
    //   from: moment(this.props.values.start_at).format('DD/MM/YYYY'),
    //   to: moment(this.props.values.finish_at).format('DD/MM/YYYY')
    // });
  }

  handleDatesChange = (startDate, endDate) => {
    this.setState(
      {
        startDate,
        endDate
      },
      async () => {
        let { startDate, endDate } = this.state;
        startDate = moment(startDate).format();
        endDate = moment(endDate).format();
        if (!isEmpty(startDate) && !isEmpty(endDate)) {
          await this.props.setValues({
            ...this.props.values,
            start_at: startDate,
            finish_at: endDate
          });
          // console.log(startDate, endDate);
          // console.log(this.props.values);
        }
      }
    );
  };

  convertImage = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          featuredImage: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  handleDrop = (file, rejectedFile) => {
    const image = file[0];
    this.convertImage(image);
    this.setState(
      {
        newImage: image
      },
      () => {
        this.props.setValues({
          ...this.props.values,
          image: this.state.newImage
        });
      }
    );
  };

  handleWYSIWYGChange = value => {
    this.props.setValues({
      ...this.props.values,
      description: value
    });
  };

  handleWYSIWYGBlur = () => {
    this.props.setTouched({
      ...this.props.touched,
      description: true
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const { values } = this.props;
    console.log(values);
    let {
      title,
      description,
      image,
      place,
      start_at,
      finish_at
    } = this.props.values;
    const { createEvent, history } = this.props;
    let eventData = new FormData();
    eventData.append('title', title);
    eventData.append('image', image);
    eventData.append('place', place);
    eventData.append('description', description);
    eventData.append('start_at', start_at);
    eventData.append('finish_at', finish_at);
    // console.log(this.props.values);
    createEvent(eventData, history);
  };

  render() {
    const { values, history, touched, errors } = this.props;
    console.log(errors);
    // react day picker input

    return (
      <form onSubmit={this.handleSubmit}>
        <Helmet>
          <title>Create a new event</title>
        </Helmet>
        <h1 className="route-title">Create a new event</h1>
        <div className="row">
          <div className="col-md-9">
            <FileUpload
              label="Upload featured image"
              onDrop={this.handleDrop}
              maxSize={1000000}
              uploadText={`<i class="fas fa-cloud-upload-alt"></i><p>Click / drag your image here</p>`}
              className="dropzone-filedrop"
            />

            <div className="form-group">
              {touched.title && errors.title && (
                <div className="invalid-feedback ">{errors.title}</div>
              )}
              <label>Title</label>
              <Field
                className="form-control"
                type="text"
                name="title"
                value={values.title}
              />
            </div>

            <div className="form-group">
              {touched.place && errors.place && (
                <div className="invalid-feedback ">{errors.place}</div>
              )}
              <label>Place</label>
              <Field
                className="form-control"
                type="text"
                name="place"
                value={values.place}
              />
            </div>

            <WYSIWYG
              onChange={this.handleWYSIWYGChange}
              value={values.description}
              modules={EventCreate.modules}
              label="Description"
              onBlur={this.handleWYSIWYGBlur}
              errorValidation={
                touched.description &&
                errors.description && (
                  <div className="invalid-feedback">{errors.description}</div>
                )
              }
            />
          </div>
          <div className="col-md-3">
            <div className="form-group">
              <label>Event Date</label>
              <DateRangePicker
                startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                displayFormat={'ddd, D MMMM YYYY'}
                startDateId={'1'} // PropTypes.string.isRequired,
                startDatePlaceholderText={'Starting date'}
                endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                endDateId={'2'} // PropTypes.string.isRequired,
                numberOfMonths={1}
                onDatesChange={({ startDate, endDate }) =>
                  this.handleDatesChange(startDate, endDate)
                } // PropTypes.func.isRequired,
                focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
              />
            </div>
            <div className="form-group">
              <label>Featured Image</label>
              {this.state.featuredImage ? (
                <div className="preview-image">
                  <img
                    alt={`blog's featured pict`}
                    src={this.state.featuredImage}
                  />
                </div>
              ) : null}
            </div>
          </div>
        </div>
        <button
          disabled={touched.title && isEmpty(errors) ? '' : 'disabled'}
          type="submit"
          className="btn btn-primary"
        >
          Create Event
        </button>
        <button
          onClick={
            (this.goBack = () => {
              history.push('/blog');
            })
          }
          className="btn btn-outline-info ml-2"
        >
          cancel
        </button>
      </form>
    );
  }
}

EventCreate.modules = {
  toolbar: [
    [{ header: '1' }, { header: '2' }],
    [{ align: [] }],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ color: [] }, { background: [] }],
    ['link', 'image'],
    ['code-block']
  ]
};

const mapDispatchToProps = {
  ...eventActions
};

const EventCreateFormik = withFormik({
  enableReinitialize: true,
  mapPropsToValues() {
    return {
      image: '',
      title: '',
      place: '',
      description: '',
      start_at: '',
      finish_at: ''
    };
  },
  validationSchema: yup.object().shape({
    // image: yup.string().required('Post image is needed'),
    title: yup.string().required('Title is required'),
    place: yup.string().required('Place is required'),
    description: yup
      .string(10)
      .required('Description is required to be filled'),
    start_at: yup.date().required('Start date is required'),
    finish_at: yup.date().required('Finish date is required')
  })
})(EventCreate);

const mapStateToProps = state => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EventCreateFormik);
