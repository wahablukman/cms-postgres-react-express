import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { withFormik, Field } from 'formik';
import FileUpload from '../components/common/FileUpload';
import * as companyProfileActions from '../redux/actions/companyProfileActions';

let yup = require('yup');

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      logo: '',
      newLogo: {}
    };
  }

  async componentDidMount() {
    const { getCompanyProfile } = this.props;
    await getCompanyProfile();
    await this.setState({
      logo: this.props.companyProfile.logo
    });
  }

  convertImage = image => {
    //convert to base64 for preview only, real image is stored in cloudinary
    const reader = new FileReader();
    reader.addEventListener(
      'load',
      () => {
        // console.log(myFileItemReader.result)
        const prevImageConversion = reader.result;
        this.setState({
          logo: prevImageConversion
        });
      },
      false
    );
    reader.readAsDataURL(image);
  };

  handleDrop = (file, rejectedFile) => {
    const image = file[0];
    this.convertImage(image);
    this.setState(
      {
        newLogo: image
      },
      () => {
        this.props.setValues({
          ...this.props.values,
          logo: this.state.newLogo
        });
      }
    );
  };

  handleSubmit = e => {
    e.preventDefault();
    const {
      name,
      email,
      facebook,
      instagram,
      youtube,
      logo
    } = this.props.values;
    const { updateCompanyProfile } = this.props;
    let postData = new FormData();
    postData.append('name', name);
    postData.append('email', email);
    postData.append('facebook', facebook);
    postData.append('instagram', instagram);
    postData.append('youtube', youtube);
    postData.append('logo', logo);
    updateCompanyProfile(postData);
  };

  render() {
    const { values } = this.props;
    console.log(this.props.companyProfile.logo);
    return (
      <div>
        <Helmet>
          <title>Settings</title>
        </Helmet>
        <h1 className="route-title">Settings</h1>
        <br />
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-4">
              <div className="form-group">
                <label>Name</label>
                <Field
                  className="form-control"
                  type="text"
                  name="name"
                  value={values.name}
                />
              </div>
              <div className="form-group">
                <label>Email</label>
                <Field
                  className="form-control"
                  type="text"
                  name="email"
                  value={values.email}
                />
              </div>
              <div className="form-group">
                <label>Facebook</label>
                <Field
                  className="form-control"
                  type="text"
                  name="facebook"
                  value={values.facebook}
                />
              </div>
              <div className="form-group">
                <label>Instagram</label>
                <Field
                  className="form-control"
                  type="text"
                  name="instagram"
                  value={values.instagram}
                />
              </div>
              <div className="form-group">
                <label>YouTube</label>
                <Field
                  className="form-control"
                  type="text"
                  name="youtube"
                  value={values.youtube}
                />
              </div>
            </div>
            <div className="col-md-3">
              <FileUpload
                label="Upload Company's logo"
                onDrop={this.handleDrop}
                maxSize={1000000}
                className="dropzone-filedrop image-prev"
                uploadText={`<img src=${this.state.logo}>`}
              />
            </div>
          </div>

          <button type="submit" className="btn btn-primary">
            Save changes
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  companyProfile: state.companyProfile
});

const mapDispatchToProps = {
  ...companyProfileActions
};

const SettingsFormik = withFormik({
  enableReinitialize: true,
  mapPropsToValues(props) {
    return {
      name: props.companyProfile.name || '',
      email: props.companyProfile.email || '',
      facebook: props.companyProfile.facebook || '',
      instagram: props.companyProfile.instagram || '',
      youtube: props.companyProfile.youtube || ''
    };
  },
  validationSchema: yup.object().shape({
    name: yup.string().required(),
    email: yup
      .string()
      .email({ minDomainSegments: 2 })
      .required(),
    facebook: yup.string().nullable(),
    instagram: yup.string().nullable(),
    youtube: yup.string().nullable()
  })
})(Settings);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsFormik);
